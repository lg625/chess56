package chess;

/**
 * Queen subclass
 * 
 * @author Jon Cobi Delfin and Luis Guzman
 */

public class Queen extends ChessPiece{
	/**
	 * Creates a queen chess piece
	 * @param type Type of the queen, can be 'w' or 'b'.
	 * @param location Current location of the queen. Coordinates are stored as rowcolumn instead of filerank
	 */
	public Queen(String type, String location) {
		super(type, location);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean movePiece(String oldLocation, String newLocation) {
		// Original Coordinates
		String s1 = convertInput(oldLocation);
		int x1 = Character.getNumericValue(s1.charAt(0)), y1 = Character.getNumericValue(s1.charAt(1));
		ChessPiece origin = ChessPiece.board[x1][y1];
		
		// New Coordinates
		String s2 = convertInput(newLocation);
		int x2 = Character.getNumericValue(s2.charAt(0)), y2 = Character.getNumericValue(s2.charAt(1));
		ChessPiece obj = ChessPiece.board[x2][y2];
		
		// Check if move is valid
		boolean isValid = false;
		boolean diag = false; //If the input is diagonal, check that the new pos is on the same line
		try {
			diag = (Math.abs((y2-y1)/(x2-x1)) == 1) ? true : false; 
		}catch(ArithmeticException e) {
			diag = false;
		}
		
		if(diag || x1 == x2 || y1 == y2){//Check if new location is on the diagonal or on the same file or rank
			//Check if input is correct
			if(origin.type.equals("wQ") && (obj == null || obj.type.charAt(0) == 'b')) {
				if(x1 != x2 && y1 == y2) {
					if(x1 > x2) {
						//Moving up the file
						for(int i = x1-1; i >= x2; i--) {
							String s = i + "" + y1;
							if(getPiece(s) != null && i != x2) return false; //piece is in the way, cannot move
						}
						isValid = true;
					}else if(x1 < x2) {
						//Moving down the file
						for(int i = x1+1; i <= x2; i++) {
							String s = i + "" + y1;
							if(getPiece(s) != null && i != x2) return false; //piece is in the way, cannot move
						}
						isValid = true;
					}
				}
				if(x1 == x2 && y1 != y2) {
					if(y1 < y2) {
						//Moving right
						for(int i = y1+1; i <= y2; i++ ) {
							String s = x1 + "" + i;
							if(getPiece(s) != null && i != y2) return false; //piece is in the way, cannot move
						}
						isValid = true;
					}else if(y1 > y2) {
						//Moving left
						for(int i = y1-1; i >= y2; i-- ) {
							String s = x1 + "" + i;
							if(getPiece(s) != null && i != y2) return false; //piece is in the way, cannot move
						}
						isValid = true;
					}
				}else {
					int j = 1;
					if(y1 < y2) {
						//Forward diagonal
						for(int i = y1+1; i <= y2; i++) {
							if(x1-j >= 0) { 
								String s = (x1-j) + "" + i;
								if(getPiece(s) != null && x1-j != x2 && i != y2) return false; //piece is in the way, cannot move
								j+=1;
							}else break; //already at max rank
						}
						isValid = true;
					}else if(y1 > y2) {
						//Backward diagonal
						for(int i = y1-1; i >= y2; i--) {
							if(x1+j < 8) { 
								String s = (x1+j) + "" + i; 
								if(getPiece(s) != null && x1+j != x2 && i != y2) return false; //piece is in the way, cannot move
								j+=1;
							}else break; //already at min rank
						}
						isValid = true;
					}
				}
			}else if(origin.type.equals("bQ") && (obj == null || obj.type.charAt(0) == 'w')) {
				if(x1 != x2 && y1 == y2) {
					if(x1 > x2) {
						//Moving up the file
						for(int i = x1-1; i >= x2; i--) {
							String s = i + "" + y1;
							if(getPiece(s) != null && i != x2) return false; //piece is in the way, cannot move
						}
						isValid = true;
					}else if(x1 < x2) {
						//Moving down the file
						for(int i = x1+1; i <= x2; i++) {
							String s = i + "" + y1;
							if(getPiece(s) != null && i != x2) return false; //piece is in the way, cannot move
						}
						isValid = true;
					}
				}
				if(x1 == x2 && y1 != y2) {
					if(y1 < y2) {
						//Moving right
						for(int i = y1+1; i <= y2; i++ ) {
							String s = x1 + "" + i;
							if(getPiece(s) != null && i != y2) return false; //piece is in the way, cannot move
						}
						isValid = true;
					}else if(y1 > y2) {
						//Moving left
						for(int i = y1-1; i >= y2; i-- ) {
							String s = x1 + "" + i;
							if(getPiece(s) != null && i != y2) return false; //piece is in the way, cannot move
						}
						isValid = true;
					}
				}else {
					int j = 1;
					if(y1 < y2) {
						//Forward diagonal
						for(int i = y1+1; i <= y2; i++) {
							if(x1-j >= 0) { 
								String s = (x1-j) + "" + i;
								if(getPiece(s) != null && x1-j != x2 && i != y2) return false; //piece is in the way, cannot move
								j+=1;
							}else break; //already at max rank
						}
						isValid = true;
					}else if(y1 > y2) {
						//Backward diagonal
						for(int i = y1-1; i >= y2; i--) {
							if(x1+j < 8) { 
								String s = (x1+j) + "" + i; 
								if(getPiece(s) != null && x1+j != x2 && i != y2) return false; //piece is in the way, cannot move
								j+=1;
							}else break; //already at min rank
						}
						isValid = true;
					}
				}
			}
		}
		// Update board location if valid
		if(isValid) {
//			System.out.println("Valid Move.");
			ChessPiece.updateBoard(s1, s2);
			return true;
		}
		else {
//			System.out.println("Invalid Move.");
			return false;
		}
	}
	
//	public static void main(String[] args) {
//		Queen q1 = new Queen("wQ", "73");
//		Queen q2 = new Queen("bQ", "03");
//		Rook r = new Rook("wR", "53");
//		
//		setPiece(q1);
//		setPiece(q2);
//		setPiece(r);
//		printBoard();
//
//		board[7][3].movePiece("d1", "d8");
//		printBoard();
//		
//		board[5][3].movePiece("d3", "e4");
//		printBoard();
//		
//		board[7][3].movePiece("d1", "d8");
//		printBoard();
//		
//		board[0][3].movePiece("d8", "h8");
//		printBoard();
//	}
}
