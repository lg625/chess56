package chess;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Main Chess Driver Function. Alternates between players turn accordingly.
 * 
 * @author Jon Cobi Delfin and Luis Guzman
 */

public class Chess {

	/*
	 * Main Chess Function. Alternates between players turn accordingly.
	 */
	public static void main(String[] args) {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		
		ChessPiece.initBoard();
		ChessPiece.printBoard();
		
		int wx = 7, wy = 4; //White king x and y coords, default 7,4
		int bx = 0, by = 4; //Black king x and y coords, default 0,4
		ArrayList<String> blockWhite = new ArrayList<String>();
		ArrayList<String> blockBlack = new ArrayList<String>();
		boolean whiteChecked = false;
		boolean blackChecked = false;
		boolean whiteOfferedDraw = false;
		boolean blackOfferedDraw = false;
		boolean isValid = false;
		
		while(true) {
			ChessPiece p = null;
			String line = "", oldPos = "", newPos = "";
			int turn = ChessPiece.getTurn();
			
			if(turn % 2 == 0) System.out.print("White's Move: ");
			else System.out.print("Black's Move: ");
			try {
				line = in.readLine().toLowerCase();
				System.out.println();
				oldPos = ChessPiece.convertInput(line.substring(0, 2));
				newPos = ChessPiece.convertInput(line.substring(3, 5));
				
				p = ChessPiece.getPiece(oldPos);
			} 
			catch(Exception e) {

			}
						
			// Check if any input is remaining after substring(3,5)
			// This input is used to specify chess piece after pawn promotion (ex. "g7 g8 N")
			if(line.length() == 7)
				newPos = newPos.concat(line.substring(6));
			
			// Perform move if valid
			if((line.equalsIgnoreCase("resign") == false && line.equalsIgnoreCase("draw") == false)) {
//				if(p == null) {
//					System.out.println("Illegal move, try again\n");
//					ChessPiece.updateTurn("--");
//				}
			} 
			if(turn % 2 == 0) {
				//White move
				if(line.equals("resign")) {
					System.out.println("Black wins");
					break;
				}else if(line.equals("draw") && blackOfferedDraw){
					//Opponent offered draw and player agreed
					break;
				}else if(line.equals("draw") && blackOfferedDraw == false){
					System.out.println("Illegal move, try again\n");
					ChessPiece.updateTurn("--");
					isValid = false;
				}else {
					if(line.contains("draw?")) {
						whiteOfferedDraw = true;
					}
					if(whiteChecked) {
						String wKPos = wx+""+wy;
						ArrayList<String> kingMoves = new ArrayList<String>();
						ChessPiece attacker = ChessPiece.isUnderAttack(wKPos, 'w');
						ChessPiece.getKingMoves(kingMoves, wKPos);
						Iterator<String> iter = kingMoves.iterator();
						while(iter.hasNext()) {
							String move = iter.next();
							if(ChessPiece.isUnderAttack(move, 'w') != null) {
								iter.remove();
							}
						}
						
						//If king can move
						if(!kingMoves.isEmpty() && kingMoves.contains(newPos)) {
							wx = Character.getNumericValue(newPos.charAt(0));
							wy = Character.getNumericValue(newPos.charAt(1));
							whiteChecked = false;
						}
						if(kingMoves.isEmpty()) {
							//If attacking piece can be taken
							if(newPos.equals(attacker.location)) whiteChecked = false;
						}
						//If attacking piece can be blocked
						int ax = Character.getNumericValue(attacker.location.charAt(0));
						int ay = Character.getNumericValue(attacker.location.charAt(1));

						int newPosx = Character.getNumericValue(newPos.charAt(0));
						int newPosy = Character.getNumericValue(newPos.charAt(1));

						if(ax-wx == 0) {
							//Attacking vertically
							if((newPosx >= ax && newPosx <= wx) || (newPosx <= ax && newPosx >= wx)) {
								whiteChecked = false;
							}
						}else if(ay-wy == 0) {
							//Attacking horizontally
							if((newPosy >= ay && newPosy <= wy) || (newPosy <= ay && newPosy >= wy)) {
								whiteChecked = false;
							}
						}else {
							//Attacking diagonally
							int diag = (ay-wy)/(ax-wx);
							if(diag > 0) {
								if(newPosy > ay && newPosx < ax && newPosy < wy && newPosx > wx) {
									whiteChecked = false;
								}
							}else if(diag < 0) {
								if(newPosy < ay && newPosx > ax && newPosy > wy && newPosx < wx) {
									whiteChecked = false;
								}
							}
						}
					}
					boolean moved = false;
					//Move the piece
					if(whiteChecked == false) {
						if(p == null) {
							System.out.println("Illegal move, try again\n");
							ChessPiece.updateTurn("--");
							isValid = false;
						}else if(p.type.charAt(0) == 'b' || p.movePiece(oldPos, newPos) == false) {
							System.out.println("Illegal move, try again\n");
							ChessPiece.updateTurn("--");
							isValid = false;
						}else{
							moved = true;
							isValid = true;
							if(p.type.charAt(1) == 'K') {
								wx = Character.getNumericValue(p.location.charAt(0));
								wy = Character.getNumericValue(p.location.charAt(1));
							}
							if(blackOfferedDraw) blackOfferedDraw = false;
						}
						//Move leaves king in check, undo
						if(moved && ChessPiece.isUnderAttack(wx+""+wy, 'w') != null) {
							try{
								p.movePiece(newPos, oldPos);
							}catch(Exception e) {
								System.out.println(e);
							}
							System.out.println("Illegal move, try again\n");
							ChessPiece.updateTurn("--");
							whiteChecked = true;
							isValid = false;
						}
					}else {
						System.out.println("Illegal move, try again\n");
						ChessPiece.updateTurn("--");
					}
					
					//Check if move put black king under check
					String bKPos = bx+""+by;
					if(moved == true && ChessPiece.isUnderAttack(bKPos, 'b') != null) {
						boolean kingStuck = false, canTake = false, canBlock = false;
						//If king can move out of check
						ArrayList<String> kingMoves = new ArrayList<String>();
						ChessPiece.getKingMoves(kingMoves, bKPos);
						Iterator<String> iter = kingMoves.iterator();
						while(iter.hasNext()) {
							String move = iter.next();
							if(ChessPiece.isUnderAttack(move, 'b') != null) {
								iter.remove();
							}
						}
						if(kingMoves.isEmpty()) kingStuck = true;
						
						//If piece can take attacker
						if(ChessPiece.isUnderAttack(newPos, 'w') != null) canTake = true;
						
						//If piece can block attacker
						int ax = Character.getNumericValue(p.location.charAt(0));
						int ay = Character.getNumericValue(p.location.charAt(1));
						blockWhite.clear();
						for(int i = 0; i < 8; i++) {
							//Vertical
							if(by == ay && bx != ax) {
								if(bx+i < ax) {
									if(ChessPiece.isUnderAttack((bx+i)+""+by, 'w') != null) {
										canBlock = true;
										blockWhite.add((bx+i)+""+by);
									}
								}else if(bx-i > ax) {
									if(ChessPiece.isUnderAttack((bx-i)+""+by, 'w') != null) {
										canBlock = true;
										blockWhite.add((bx-i)+""+by);
									}
								}
							}
							//Horizontal
							if(bx == ax && by != ay) {
								if(by+i < ay) {
									if(ChessPiece.isUnderAttack(bx+""+(by+i), 'w') != null) {
										canBlock = true;
										blockWhite.add(bx+""+(by+i));
									}
								}else if(by-i > ay) {
									if(ChessPiece.isUnderAttack(bx+""+(by-i), 'w') != null) {
										canBlock = true;
										blockWhite.add(bx+""+(by-i));
									}
								}
							}
							//Diagonal
							if(bx+i < ax && by+i < ay) {
								if(ChessPiece.isUnderAttack((bx+i)+""+(by+i), 'w') != null) {
									canBlock = true;
									blockWhite.add((bx+i)+""+(by+i));
								}
							}
							if(bx-i > ax && by+i < ay) {
								if(ChessPiece.isUnderAttack((bx-i)+""+(by+i), 'w') != null) {
									canBlock = true;
									blockWhite.add((bx-i)+""+(by+i));
								}
							}
							if(bx+i < ax && by-i > ay) {
								if(ChessPiece.isUnderAttack((bx+i)+""+(by-i), 'w') != null) {
									canBlock = true;
									blockWhite.add((bx+i)+""+(by-i));
								}
							}
							if(bx-i > ax && by-i > ay) {
								if(ChessPiece.isUnderAttack((bx-i)+""+(by-i), 'w') != null) {
									canBlock = true;
									blockWhite.add((bx-i)+""+(by-i));
								}
							}
						}
						Iterator<String> it = blockWhite.iterator();
						while(it.hasNext()) {
							String move = it.next();
							if(ChessPiece.isUnderAttack(move, 'w') != null) {
								it.remove();
							}
						}
						if(blockWhite.isEmpty()) canBlock = false;
						if(kingStuck && !canTake && !canBlock) {
							System.out.println("Checkmate. White wins");
							break;
						}else{
							System.out.println("Check\n");
							blackChecked = true;
						}
					}
				}
			} 
			else {
				//Black move
				if(line.equals("resign")) {
					System.out.println("White wins");
					break;
				}else if(line.equals("draw") && whiteOfferedDraw){
					//Opponent offered draw and player agreed
					break;
				}else if(line.equals("draw") && whiteOfferedDraw == false){
					System.out.println("Illegal move, try again\n");
					ChessPiece.updateTurn("--");
					isValid = false;
				}else {
					if(line.contains("draw?")) {
						blackOfferedDraw = true;
					}
					if(blackChecked) {
						String bKPos = bx+""+by;
						ArrayList<String> kingMoves = new ArrayList<String>();
						ChessPiece attacker = ChessPiece.isUnderAttack(bKPos, 'b');
						ChessPiece.getKingMoves(kingMoves, bKPos);
						Iterator<String> iter = kingMoves.iterator();
						while(iter.hasNext()) {
							String move = iter.next();
							if(ChessPiece.isUnderAttack(move, 'b') != null) {
								iter.remove();
							}
						}
						
						//If king can move
						if(!kingMoves.isEmpty() && kingMoves.contains(newPos)) {
							bx = Character.getNumericValue(newPos.charAt(0));
							by = Character.getNumericValue(newPos.charAt(1));
							blackChecked = false;
						}
						if(kingMoves.isEmpty()) {
							//If attacking piece can be taken
							if(newPos.equals(attacker.location)) blackChecked = false;
						}
						//If attacking piece can be blocked
						int ax = Character.getNumericValue(attacker.location.charAt(0));
						int ay = Character.getNumericValue(attacker.location.charAt(1));

						int newPosx = Character.getNumericValue(newPos.charAt(0));
						int newPosy = Character.getNumericValue(newPos.charAt(1));

						if(ax-bx == 0) {
							//Attacking vertically
							if((newPosx >= ax && newPosx <= bx) || (newPosx <= ax && newPosx >= bx)) {
								blackChecked = false;
							}
						}else if(ay-by == 0) {
							//Attacking horizontally
							if((newPosy >= ay && newPosy <= by) || (newPosy <= ay && newPosy >= by)) {
								blackChecked = false;
							}
						}else {
							//Attacking diagonally
							int diag = (ay-by)/(ax-bx);
							if(diag > 0) {
								if(newPosy > ay && newPosx < ax && newPosy < by && newPosx > bx) {
									blackChecked = false;
								}
							}else if(diag < 0) {
								if(newPosy < ay && newPosx > ax && newPosy > by && newPosx < bx) {
									blackChecked = false;
								}
							}
						}
					}
					
					boolean moved = false;
					//Move the piece
					if(blackChecked == false) {
						if(p == null) {
							System.out.println("Illegal move, try again\n");
							ChessPiece.updateTurn("--");
							isValid = false;
						}else if(p.type.charAt(0) == 'w' || p.movePiece(oldPos, newPos) == false) {
							System.out.println("Illegal move, try again\n");
							ChessPiece.updateTurn("--");
							isValid = false;
						}else{
							moved = true;
							isValid = true;
							if(p.type.charAt(1) == 'K') {
								bx = Character.getNumericValue(p.location.charAt(0));
								by = Character.getNumericValue(p.location.charAt(1));
							}
							if(whiteOfferedDraw) whiteOfferedDraw = false;//rejected draw offer
						}
						//Move leaves king in check, undo
						if(moved && ChessPiece.isUnderAttack(bx+""+by, 'b') != null) {
							p.movePiece(newPos, oldPos);
							System.out.println("Illegal move, try again\n");
							ChessPiece.updateTurn("--");
							whiteChecked = true;
							isValid = false;
						}
					}else {
						System.out.println("Illegal move, try again\n");
						ChessPiece.updateTurn("--");
					}
					
					//Check if move put white king under check
					String wKPos = wx+""+wy;
					if(moved == true && ChessPiece.isUnderAttack(wKPos, 'w') != null) {
						boolean kingStuck = false, canTake = false, canBlock = false;
						//If king can move out of check
						ArrayList<String> kingMoves = new ArrayList<String>();
						ChessPiece.getKingMoves(kingMoves, wKPos);
						Iterator<String> iter = kingMoves.iterator();
						while(iter.hasNext()) {
							String move = iter.next();
							if(ChessPiece.isUnderAttack(move, 'w') != null) {
								iter.remove();
							}
						}
						if(kingMoves.isEmpty()) kingStuck = true;
						
						//If piece can take attacker
						if(ChessPiece.isUnderAttack(newPos, 'b') != null) canTake = true;
						
						//If piece can block attacker
						int ax = Character.getNumericValue(p.location.charAt(0));
						int ay = Character.getNumericValue(p.location.charAt(1));
						blockBlack.clear();
						for(int i = 0; i < 8; i++) {
							//Vertical
							if(wy == ay && wx != ax) {
								if(wx+i < ax) {
									if(ChessPiece.isUnderAttack((wx+i)+""+wy, 'b') != null) {
										canBlock = true;
										blockBlack.add((wx+i)+""+wy);
									}
								}else if(wx-i > ax) {
									if(ChessPiece.isUnderAttack((wx-i)+""+wy, 'b') != null) {
										canBlock = true;
										blockBlack.add((wx-i)+""+wy);
									}
								}
							}
							//Horizontal
							if(wx == ax && wy != ay) {
								if(wy+i < ay) {
									if(ChessPiece.isUnderAttack(wx+""+(wy+i), 'b') != null) {
										canBlock = true;
										blockBlack.add(wx+""+(wy+i));
									}
								}else if(wy-i > ay) {
									if(ChessPiece.isUnderAttack(wx+""+(wy-i), 'b') != null) {
										canBlock = true;
										blockBlack.add(wx+""+(wy-i));
									}
								}
							}
							//Diagonal
							if(wx+i < ax && wy+i < ay) {
								if(ChessPiece.isUnderAttack((wx+i)+""+(wy+i), 'b') != null) {
									canBlock = true;
									blockBlack.add((wx+i)+""+(wy+i));
								}
							}
							if(wx-i > ax && wy+i < ay) {
								if(ChessPiece.isUnderAttack((wx-i)+""+(wy+i), 'b') != null) {
									canBlock = true;
									blockBlack.add((wx-i)+""+(wy+i));
								}
							}
							if(wx+i < ax && wy-i > ay) {
								if(ChessPiece.isUnderAttack((wx+i)+""+(wy-i), 'b') != null) {
									canBlock = true;
									blockBlack.add((wx+i)+""+(wy-i));
								}
							}
							if(wx-i > ax && wy-i > ay) {
								if(ChessPiece.isUnderAttack((wx-i)+""+(wy-i), 'b') != null) {
									canBlock = true;
									blockBlack.add((wx-i)+""+(wy-i));
								}
							}
						}
						Iterator<String> it = blockBlack.iterator();
						while(it.hasNext()) {
							String move = it.next();
							if(ChessPiece.isUnderAttack(move, 'b') != null) {
								it.remove();
							}
						}
						if(blockBlack.isEmpty()) canBlock = false;
						if(kingStuck && !canTake && !canBlock) {
							System.out.println("Checkmate. Black wins");
							break;
						}else{
							System.out.println("Check\n");
							whiteChecked = true;
						}
					}
					
				}
			}
			if(isValid == false) {
				ChessPiece.updateTurn("--");
			}else {
				ChessPiece.printBoard();
				ChessPiece.updateTurn("++");
			}
		}
	}
}
