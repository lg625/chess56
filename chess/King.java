package chess;

/**
 * King subclass
 * 
 * @author Jon Cobi Delfin and Luis Guzman
 */

public class King extends ChessPiece{
	/**
	 * Specify if the king has moved. Used to determine if castling is allowed
	 */
	public boolean kingHasMoved = false;
	/**
	 * Creates a king chess piece 
	 * @param type Type of the king, can be 'w' or 'b'.
	 * @param location Current location of the king. Coordinates are stored as rowcolumn instead of filerank
	 */
	public King(String type, String location) {
		super(type, location);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean movePiece(String oldLocation, String newLocation) {
		// Original Coordinates
		String s1 = convertInput(oldLocation);
		int x1 = Character.getNumericValue(s1.charAt(0)), y1 = Character.getNumericValue(s1.charAt(1));
		ChessPiece origin = ChessPiece.board[x1][y1];
		
		// New Coordinates
		String s2 = convertInput(newLocation);
		int x2 = Character.getNumericValue(s2.charAt(0)), y2 = Character.getNumericValue(s2.charAt(1));
		ChessPiece obj = ChessPiece.board[x2][y2];
		
		// Check if move is valid
		boolean isValid = false;
		boolean diag = false;
		if(x1+1 == x2 || x1-1 == x2) {
			if(y1+1 == y2 || y1-1 == y2) diag = true;
		}
		if(diag || (x1+1 == x2 && y1 == y2) || (x1-1 == x2 && y1 == y2) || (x1 == x2 && y1+1 == y2) || (x1 == x2 && y1-1 == y2)) {
			if(origin.type.equals("wK") && (obj == null || obj.type.charAt(0) == 'b')) {
				if(isUnderAttack(s2,'w') == null) isValid = true;
			}else if(origin.type.equals("bK") && (obj == null || obj.type.charAt(0) == 'w')) {
				if(isUnderAttack(s2, 'b') == null) isValid = true; 
			}
		}else if(kingHasMoved == false && obj != null && (obj.type.equals("wR") || obj.type.equals("bR"))) {
			/* Castle input
			 * Castling Rules:
			 * 	1. King has not moved
			 *  2. Rook has not moved
			 *  3. No pieces in between king and rook
			 *  4. Squares in between king and rook are not being attacked
			 *  5. King is not in check after castling 
			 */
			if(origin.type.equals("wK") && ((Rook)obj).rookHasMoved == false) {
				if(isUnderAttack("74", 'w') != null) return false; //Check if king is under attack
				if(x2 == 7 && y2 == 0) { //a1
//					System.out.println("Long castle white");
					//Long castle
					for(int i = y1-1; i > y2; i--) {
						String s = x2 + "" + i;
						if(getPiece(s) != null) return false; //Check for empty square
						if(isUnderAttack(s, 'w') != null) return false; //Check is square is under attack
					}
					this.kingHasMoved = true;
					((Rook)obj).rookHasMoved = true;
					updateBoard("74", "72"); //Update king position
					updateBoard("70", "73"); //Update rook position
					return true;
				}else if(x2 == 7 && y2 == 7) { //h1
//					System.out.println("Short castle white");
					//Short castle
					for(int i = y1+1; i < y2; i++) {
						String s = x2 + "" + i;
						if(getPiece(s) != null) return false; //Check for empty square
						if(isUnderAttack(s, 'w') != null) return false; //Check is square is under attack
					}
					this.kingHasMoved = true;
					((Rook)obj).rookHasMoved = true;
					updateBoard("74", "76"); //Update king position
					updateBoard("77", "75"); //Update rook position
					return true;
				}
			}else if(origin.type.equals("bK") && ((Rook)obj).rookHasMoved == false) {
				if(isUnderAttack("04", 'b') != null) return false; //Check if king is under attack
				if(x2 == 0 && y2 == 0) { //a8
//					System.out.println("Long castle black");
					//Long Castle
					for(int i = y1-1; i > y2; i--) {
						String s = x2 + "" + i;
						if(getPiece(s) != null) return false; //Check for empty square
						if(isUnderAttack(s, 'b') != null) return false; //Check is square is under attack
					}
					this.kingHasMoved = true;
					((Rook)obj).rookHasMoved = true;
					updateBoard("04", "02"); //Update king position
					updateBoard("00", "03"); //Update rook position
					return true;
				}else if(x2 == 0 && y2 == 7) {//h8
//					System.out.println("Short castle black");
					//Short Castle
					for(int i = y1+1; i < y2; i++) {
						String s = x2 + "" + i;
						if(getPiece(s) != null) return false; //Check for empty square
						if(isUnderAttack(s, 'b') != null) return false; //Check is square is under attack
					}
					this.kingHasMoved = true;
					((Rook)obj).rookHasMoved = true;
					updateBoard("04", "06"); //Update king position
					updateBoard("07", "05"); //Update rook position
					return true;
				}
			}
		}
		// Update board location if valid
		if(isValid) {
//			System.out.println("Valid Move.");
			ChessPiece.updateBoard(s1, s2);
			return true;
		}
		else {
//			System.out.println("Illegal Move.");
			return false;
		}
	}
	
//	public static void main(String[] args) {
//		King k1 = new King("wK", "74");
//		King k2 = new King("bK", "04");
//		Rook r1 = new Rook("wR", "70");
//		Rook r2 = new Rook("wR", "77");
//		Rook r3 = new Rook("bR", "00");
//		Rook r4 = new Rook("bR", "07");
//		Queen q = new Queen("bQ", "24");
//		
//		setPiece(k1);
//		setPiece(k2);
//		setPiece(r1);
//		setPiece(r2);
//		setPiece(r3);
//		setPiece(r4);
//		setPiece(q);
////		
//		printBoard();
//		board[7][4].movePiece("e1", "a1"); //long castle white
////		board[7][4].movePiece("e1",	"h1"); //short castle white
////		
////		board[0][4].movePiece("e8", "a8"); //long castle black
////		board[0][4].movePiece("e8", "h8"); //short castle black
//		printBoard();
//	}
	
}
