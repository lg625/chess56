package chess;

/**
 * Rook subclass
 * 
 * @author Jon Cobi Delfin and Luis Guzman
 */

public class Rook extends ChessPiece{
	/**
	 * Specify if the rook has moved. Used to determine if castling is allowed.
	 */
	public boolean rookHasMoved = false;
	/**
	 * Creates a rook chess piece
	 * @param type Type of the rook, can be 'w' or 'b'.
	 * @param location Current location of the rook. Coordinates are stored as rowcolumn instead of filerank
	 */
	public Rook(String type, String location) {
		// type = bRook or wRook
		super(type, location);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean movePiece(String oldLocation, String newLocation) {
		// Original Coordinates
		String s1 = convertInput(oldLocation);
		int x1 = Character.getNumericValue(s1.charAt(0)), y1 = Character.getNumericValue(s1.charAt(1));
		ChessPiece origin = ChessPiece.getPiece(s1);
		
		// New Coordinates
		String s2 = convertInput(newLocation);
		int x2 = Character.getNumericValue(s2.charAt(0)), y2 = Character.getNumericValue(s2.charAt(1));
		ChessPiece obj = ChessPiece.getPiece(s2);

		// Check if move is valid
		boolean isValid = false;
		
		// wRook and bRook both have the same functionality, the code below works for both types
		if(x1 == x2 && y1 != y2) {
			// Lateral Movement
			boolean moveRight = y2 > y1 ? true : false;
			int distance = y2 - y1;
			
			// If distance equal 1 or -1 -> no coordinates exist between origin and new
			if(distance == 1 || distance == -1) {
				isValid = true;
			}
			ChessPiece t;
			
			// Check to see if any pieces are between coordinates
			while(distance != 1 && distance != -1) {				
				//System.out.println("Checking coordinate: " + x1 + ", " + (y1+distance) + "  distance = " + distance);
				if(moveRight) {
					t = ChessPiece.getPiece(x1 + "" + (y1 + distance--));
				}
				else {
					t = ChessPiece.getPiece(x1 + "" + (y1 + distance++));
				}
				
				if(t != null && !t.equals(obj)) {
//					System.out.println("\tChess Piece found in between the path!");
					isValid = false;
					break;
				}
				else {
					isValid = true;
				}
			}
		}
		else if(x1 != x2 && y1 == y2){
			// Forward / Backward Movement
			boolean moveUp = x1 > x2 ? true : false;
			int distance = x2 - x1;
			
			if(distance == 1 || distance == -1) {
				isValid = true;
			}
			ChessPiece t;
			
			// Check to see if any pieces are between coordinates
			while(distance != 1 && distance != -1) {				
				//System.out.println("Checking coordinate: " + (x1+distance) + ", " + y1 + "  distance = " + distance);
				if(moveUp) {
					t = ChessPiece.getPiece((x1 + distance++) + "" + y1);
				}
				else {
					t = ChessPiece.getPiece((x1 + distance--) + "" + y1);
				}
				
				if(t != null && !t.equals(obj)) {
//					System.out.println("\tChess Piece found in between path!");
					isValid = false;
					break;
				}
				else
					isValid = true;
			}
		}
		
		// Update board location if valid
		if(isValid) {
			if(onSameTeam(origin, obj) == false) {
//				System.out.println("Valid Move.");
				if(obj != null) {
//					System.out.println("Opponent Captured!");
				}
				ChessPiece.updateBoard(s1, s2);
				rookHasMoved = true;
				return true;
			}
			else {
//				System.out.println("Invalid Move, cannot capture your own team");
				return false;
			}
		}
		else {
//			System.out.println("Invalid Move. Within Rook Class...");
			return false;
		}
	}

}
