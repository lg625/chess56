package chess;
/**
 * Interface used by the chess pieces. 
 * @author Jon Cobi Delfin and Luis Guzman
 *
 */
public interface PieceInterface {
	/**
	 * 
	 * @param oldLocation The current location of the piece.
	 * @param newLocation The new location of the piece.
	 * @return boolean True if the move was valid, false otherwise.
	 */
	public boolean movePiece(String oldLocation, String newLocation);
}
