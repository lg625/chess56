package chess;

/**
 * Bishop subclass
 * 
 * @author Jon Cobi Delfin and Luis Guzman
 */

public class Bishop extends ChessPiece{
	/**
	 * Creates a bishop chess piece
	 * @param type Type of the bishop, can be 'w' or 'b'.
	 * @param location Current location of the bishop. Coordinates are stored as rowcolumn instead of filerank
	 */
	public Bishop(String type, String location) {
		super(type, location);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean movePiece(String oldLocation, String newLocation) {
		// Original Coordinates
		String s1 = convertInput(oldLocation);
		int x1 = Character.getNumericValue(s1.charAt(0)), y1 = Character.getNumericValue(s1.charAt(1));
		ChessPiece origin = ChessPiece.board[x1][y1];
		
		// New Coordinates
		String s2 = convertInput(newLocation);
		int x2 = Character.getNumericValue(s2.charAt(0)), y2 = Character.getNumericValue(s2.charAt(1));
		ChessPiece obj = ChessPiece.board[x2][y2];
		
		// Check if move is valid
		boolean isValid = false;
		boolean diag = false; //If the input is diagonal, check that the new pos is on the same line
		try {
			diag = (Math.abs((y2-y1)/(x2-x1)) == 1) ? true : false; 
		}catch(ArithmeticException e) {
			diag = false;
		}
		
		if(diag){//Check if new location is on the diagonal
			//Check if input is correct
			if(origin.type.equals("wB") && (obj == null || obj.type.charAt(0) == 'b')) {
				int j = 1;
				if(y1 < y2) {
					//Forward diagonal
					for(int i = y1+1; i <= y2; i++) {
						if(x1-j >= 0) { 
							String s = (x1-j) + "" + i;
							if(getPiece(s) != null && x1-j != x2 && i != y2) return false; //piece is in the way, cannot move
							j+=1;
						}else break; //already at max rank
					}
					isValid = true;
				}else if(y1 >= y2) {
					//Backward diagonal
					for(int i = y1-1; i >= y2; i--) {
						if(x1+j < 8) { 
							String s = (x1+j) + "" + i; 
							if(getPiece(s) != null && x1+j != x2 && i != y2) return false; //piece is in the way, cannot move
							j+=1;
						}else break; //already at min rank
					}
					isValid = true;
				}
			}else if(origin.type.equals("bB") && (obj == null || obj.type.charAt(0) == 'w')) {
				int j = 1;
				if(y1 < y2) {
					//Forward diagonal
					for(int i = y1+1; i <= y2; i++) {
						if(x1-j >= 0) { 
							String s = (x1-j) + "" + i;
							if(getPiece(s) != null && x1-j != x2 && i != y2) return false; //piece is in the way, cannot move
							j+=1;
						}else break; //already at max rank
					}
					isValid = true;
				}else if(y1 > y2) {
					//Backward diagonal
					for(int i = y1-1; i >= y2; i--) {
						if(x1+j < 8) { 
							String s = (x1+j) + "" + i; 
							if(getPiece(s) != null && x1+j != x2 && i != y2) return false; //piece is in the way, cannot move
							j+=1;
						}else break; //already at min rank
					}
					isValid = true;
				}
			}
		}
		// Update board location if valid
		if(isValid) {
//			System.out.println("Valid Move.");
			ChessPiece.updateBoard(s1, s2);
			return true;
		}
		else {
//			System.out.println("Invalid Move.");
			return false;
		}
	}
//	public static void main(String[] args) {
//		Bishop b1 = new Bishop("bB", "27");
//		Bishop b2 = new Bishop("wB", "72");
//		Pawn p = new Pawn("wP", "63");
//		setPiece(b1);
//		setPiece(b2);
//		setPiece(p);
//		
//		printBoard();
//		board[7][2].movePiece("c1", "h6");
//		printBoard();
//		
//		board[2][7].movePiece("h6", "g7");
//		printBoard();
//	}
}
