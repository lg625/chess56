package chess;

import java.util.ArrayList;
/**
 * Superclass ChessPiece. Every chess piece extends from the ChessPiece class.
 * 
 * @author Jon Cobi Delfin and Luis Guzman
 */

public abstract class ChessPiece implements PieceInterface{
	/**
	 * Specify Chess Piece type, begins with either 'w' or 'b'
	 */
	String type;
	/**
	 * Location within the 2D board array. Location is based on 0 index values "row column", not "FileRank FileRank"
	 */
	String location;
	/**
	 * Shared global turn count. Used specifically to determine which players turn it currently is.
	 * Also used for enpassant checking on pawn movements.
	 */
	private static int turn = 0;
	
	/**
	 * Game board for where ChessPiece objects are stored.
	 */
	static ChessPiece[][] board = new ChessPiece[8][8];
	
	/**
	 * Super Chess Piece Constructor
	 * 
	 * @param type Specify Chess Piece type, begins with either "w" or "b".
	 * @param location Location within the 2D board array. Location is based on 0 index values "row column", not "FileRank FileRank".
	 */
	public ChessPiece(String type, String location) {
		this.type = type;
		this.location = location;
	}
	/**
	 * Initialize game board with each chess pieces at their corresponding location
	 */
	public static void initBoard() {
		//Initialize black side
		setPiece(new Rook("bR", "00"));
		setPiece(new Knight("bN", "01"));
		setPiece(new Bishop("bB", "02"));
		setPiece(new Queen("bQ", "03"));
		setPiece(new King("bK", "04"));
		setPiece(new Bishop("bB", "05"));
		setPiece(new Knight("bN", "06"));
		setPiece(new Rook("bR", "07"));
		for(int i = 0; i < 8; i++) {
			setPiece(new Pawn("bp", "1".concat(Integer.toString(i))));
		}
		
		//Initialize white side
		setPiece(new Rook("wR", "70"));
		setPiece(new Knight("wN", "71"));
		setPiece(new Bishop("wB", "72"));
		setPiece(new Queen("wQ", "73"));
		setPiece(new King("wK", "74"));
		setPiece(new Bishop("wB", "75"));
		setPiece(new Knight("wN", "76"));
		setPiece(new Rook("wR", "77"));
		for(int i = 0; i < 8; i++) {
			setPiece(new Pawn("wp", "6".concat(Integer.toString(i))));
		}
	}
	
	/**
	 * Get piece at location c from game board. Function cannot be altered by subclasses.
	 * 
	 * @param c Coordinate index of which position to retrieve Chess Piece from
	 * @return ChessPiece If object exists at position c, it'll return it. Otherwise it'll return null, such that no piece exists at position c
	 */
	public static final ChessPiece getPiece(String c) {
		int x = Character.getNumericValue(c.charAt(0)), y = Character.getNumericValue(c.charAt(1));
		// Protects out of bounds error from happening
		if(x >= 8 || x < 0 || y >= 8 || y < 0)
			return null;
		return board[x][y];
	}
	
	/**
	 * Set piece at location p.location into the game board.
	 * Function cannot be altered by subclasses
	 * 
	 * @param p Chess Piece for which to insert into the game board. Uses p.location to determine where to insert.
	 */
	public static final void setPiece(ChessPiece p) {
		int x = Character.getNumericValue(p.location.charAt(0)), y = Character.getNumericValue(p.location.charAt(1));
		board[x][y] = p;
	}
	
	/**
	 * Update board by swapping ChessPiece at oldLocatoin with ChessPiece at newLocation.
	 * Function cannot be altered by subclasses.
	 * 
	 * @param oldLocation Old / original location within game board of ChessPiece.
	 * @param newLocation New location to swap old location into
	 */
	public static final void updateBoard(String oldLocation, String newLocation) {
		int x1 = Character.getNumericValue(oldLocation.charAt(0)), y1 = Character.getNumericValue(oldLocation.charAt(1));
		int x2 = Character.getNumericValue(newLocation.charAt(0)), y2 = Character.getNumericValue(newLocation.charAt(1));
		
		board[x2][y2] = board[x1][y1];
		board[x1][y1] = null;
		
		board[x2][y2].location = newLocation;
	}
	/**
	 * Remove ChessPiece from game board at position c. Sets position c equal to null.
	 * Function cannot be altered by subclasses.
	 * 
	 * @param c Coordinate index of which position to remove Chess Piece from
	 */
	public static final void removePiece(String c) {
		int x = Character.getNumericValue(c.charAt(0)), y = Character.getNumericValue(c.charAt(1));
		board[x][y] = null;
	}
	
	/**
	 * Properly display game board
	 * Function cannot be altered by subclasses
	 */
	public static final void printBoard() {
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				String pos = Integer.toString(i).concat(Integer.toString(j));
				String piece = (getPiece(pos) == null) ? null : getPiece(pos).type;
				if(piece == null) {
					if((i % 2 == 0 && j % 2 == 1) || (i % 2 == 1 && j % 2 == 0)) {
						System.out.print("## ");
					}else System.out.print("   ");
				}else System.out.print(piece + " ");
			}
			System.out.print(8-i);
			System.out.println();
		}
		//Print the line of files
		for(int i = 0; i < 8; i++) {
			System.out.print(" " + (char)('a'+ i) + " ");
		}
		System.out.println("\n");
	}
	
	/**
	 * Determine if ChessPiece object is equal to Object o by comparing type and location fields.
	 * Overrides original equals method.
	 * 
	 * @param o Object o
	 * @return True if objects are equivalent (same type and same location), False if not equivalent.
	 */
	public boolean equals(Object o) {
		if(!(o instanceof ChessPiece) || o == null)
			return false;
		
		ChessPiece p = (ChessPiece)o;
		if(this.type.equals(p.type) && this.location.equals(p.location))
			return true;
		else
			return false;
	}
	
	/**
	 * Get global turn count
	 * Function cannot be altered by subclasses
	 * 
	 * @return int returned represents total number of turns that have gone by.
	 */
	public static final int getTurn() {
		return turn;
	}
	
	/**
	 * Update global turn count
	 * Function cannot be altered by subclasses
	 * 
	 * @param operator Either "++" to increment turn counter or "--" to decrement turn counter.
	 */
	public static final void updateTurn(String operator) {
		if(operator.equals("++"))
			turn++;
		else if(operator.equals("--"))
			turn--;
	}
	/** 
	 * Checks the first character, "b" or "w", of p1 and p2 to determine if on same team or not.
	 * Function cannot be altered by subclasses.
	 * 
	 * @param p1 ChessPiece object 1.
	 * @param p2 ChessPiece object 2.
	 * @return true if pieces are on the same team, false otherwise
	 */
	public static final boolean onSameTeam(ChessPiece p1, ChessPiece p2) {
		if(p1 == null || p2 == null)
			return false;
		
		Character c1 = p1.type.charAt(0), c2 = p2.type.charAt(0);
		return c1.equals(c2) ? true : false;
	}
	/**
	 * Convert input from "FileRank FileRank" to zero index base "Row column".
	 * Function cannot be altered by subclasses.
	 * 
	 * @param input Original "FileRank FileRank" input.
	 * @return String converted input as zero index based.
	 */
	public static final String convertInput(String input) {
		// Check to see if input is already in digit format
		if(Character.isDigit(input.charAt(0)) && Character.isDigit(input.charAt(1)))
			return input;
		
		String output = "";
		int col = (int)input.charAt(0) - 97;
		int row = 8 - Integer.parseInt(Character.toString(input.charAt(1)));
		
		output = output.concat(row + "" + col);
		return output;
	}
	
	/**
	 * Determines whether a piece is under attack.
	 * Function cannot be altered by subclasses.
	 * 
	 * @param pos Position of the piece being examined
	 * @param color Color of the piece being examined. 
	 * @return ChessPiece piece that is attacking the piece as "pos"
	 */
	public static final ChessPiece isUnderAttack(String pos, char color) {
		int x = Character.getNumericValue(pos.charAt(0)), y = Character.getNumericValue(pos.charAt(1));
		ChessPiece square = null;
		for(int i = 0; i < 8; i++) {
			//Forward right diagonal
			if(x-i >= 0 && y+i < 8) {
				square = board[x-i][y+i];
				if(square != null && square != board[x][y]) {
					if(square.type.charAt(0) == color) break;
					if(i == 1 && square.type.charAt(0) != color && square.type.charAt(1) == 'p') return square; //Attacked by pawn
					if(square.type.charAt(0) != color && (square.type.charAt(1) == 'Q' || square.type.charAt(1) == 'B')) return square; //Attacked by queen or bishop
				}
			}
		}
		for(int i = 0; i < 8; i++) {
			//Forward left diagonal
			if(x-i >= 0 && y-i >= 0) {
				square = board[x-i][y-i];
				if(square != null && square != board[x][y]) {
					if(square.type.charAt(0) == color) break;
					if(i == 1 && square.type.charAt(0) != color && square.type.charAt(1) == 'p') return square; //Attacked by pawn
					if(square.type.charAt(0) != color && (square.type.charAt(1) == 'Q' || square.type.charAt(1) == 'B')) return square; //Attacked by queen or bishop
				}
			}
		}
		for(int i = 0; i < 8; i++) {
			//Backward right diagonal
			if(x+i < 8 && y+i < 8) {
				square = board[x+i][y+i];
				if(square != null && square != board[x][y]) {
					if(square.type.charAt(0) == color) break;
					if(i == 1 && square.type.charAt(0) != color && square.type.charAt(1) == 'p') return square; //Attacked by pawn
					if(square.type.charAt(0) != color && (square.type.charAt(1) == 'Q' || square.type.charAt(1) == 'B')) return square; //Attacked by queen or bishop
				}
			}
		}
		for(int i = 0; i < 8; i++) {
			//Backward left diagonal
			if(x+i < 8 && y-i >= 0) {
				square = board[x+i][y-i];
				if(square != null && square != board[x][y]) {
					if(square.type.charAt(0) == color) break;
					if(i == 1 && square.type.charAt(0) != color && square.type.charAt(1) == 'p') return square; //Attacked by pawn
					if(square.type.charAt(0) != color && (square.type.charAt(1) == 'Q' || square.type.charAt(1) == 'B')) return square; //Attacked by queen or bishop
				}
			}
		}
		for(int i = 0; i < 8; i++) {
			//Forward file
			if(x-i >= 0) {
				square = board[x-i][y];
				if(square != null && square != board[x][y]) {
					if(square.type.charAt(0) == color) break;
					if(square.type.charAt(0) != color && (square.type.charAt(1) == 'Q' || square.type.charAt(1) == 'R')) return square; //Attacked by queen or rook
				}
			}
		}
		for(int i = 0; i < 8; i++) {
			//Backward file
			if(x+i < 8) {
				square = board[x+i][y];
				if(square != null && square != board[x][y]) {
					if(square.type.charAt(0) == color) break;
					if(square.type.charAt(0) != color && (square.type.charAt(1) == 'Q' || square.type.charAt(1) == 'R')) return square; //Attacked by queen or rook
				}
			}
		}
		for(int i = 0; i < 8; i++) {
			//Right rank
			if(y-i >= 0) {
				square = board[x][y-i];
				if(square != null && square != board[x][y]) {
					if(square.type.charAt(0) == color) break;
					if(square.type.charAt(0) != color && (square.type.charAt(1) == 'Q' || square.type.charAt(1) == 'R')) return square; //Attacked by queen or rook
				}
			}
		}
		for(int i = 0; i < 8; i++) {
			//Left rank
			if(y+i < 8) {
				square = board[x][y+i];
				if(square != null && square != board[x][y]) {
					if(square.type.charAt(0) == color) break;
					if(square.type.charAt(0) != color && (square.type.charAt(1) == 'Q' || square.type.charAt(1) == 'R')) return square; //Attacked by queen or rook
				}
			}
		}
		for(int i = 0; i < 8; i++) {
			//Check for knights
			if(x-2 >= 0) {
				//Up 2 right 1
				if(y+i+1 < 8) {
					square = board[x-2][y+i+1];
					if(square != null) {
						if(square.type.charAt(0) != color && square.type.charAt(1) == 'K') return square; //Attacked by knight
					}
				}
				//Up 2 left 1
				if(y-i-1 >= 0) {
					square = board[x-2][y-i-1];
					if(square != null) {
						if(square.type.charAt(0) != color && square.type.charAt(1) == 'K') return square; //Attacked by knight
					}
				}
			}
			if(x+2 < 8) {
				//Down 2 right 1
				if(y+i+1 < 8) {
					square = board[x+2][y+i+1];
					if(square != null) {
						if(square.type.charAt(0) != color && square.type.charAt(1) == 'K') return square; //Attacked by knight
					}
				}
				//Down 2 left 1
				if(y-i-1 >= 0) {
					square = board[x+2][y-i-1];
					if(square != null) {
						if(square.type.charAt(0) != color && square.type.charAt(1) == 'K') return square; //Attacked by knight
					}
				}
			}
			if(x-1 >= 0) {
				//Up 1 right 2
				if(y+i+2 < 8) {
					square = board[x-1][y+i+2];
					if(square != null) {
						if(square.type.charAt(0) != color && square.type.charAt(1) == 'K') return square; //Attacked by knight
					}
				}
				//Up 1 left 2
				if(y-i-2 >= 0) {
					square = board[x-1][y-i-2];
					if(square != null) {
						if(square.type.charAt(0) != color && square.type.charAt(1) == 'K') return square; //Attacked by knight
					}
				}
			}
			if(x+1 < 8) {
				//Down 1 right 2
				if(y+i+2 < 8) {
					square = board[x+1][y+i+2];
					if(square != null) {
						if(square.type.charAt(0) != color && square.type.charAt(1) == 'K') return square; //Attacked by knight
					}
				}
				//Down 1 left 2
				if(y-i-2 >= 0) {
					square = board[x+1][y-i-2];
					if(square != null) {
						if(square.type.charAt(0) != color && square.type.charAt(1) == 'K') return square; //Attacked by knight
					}
				}
			}
		}
		return null;
	}
	
	/**
	 * Get the available moves the king can currently execute
	 * Function cannot be altered by subclasses.
	 * 
	 * @param list Empty list to be populated with potential moves the king can execute
	 * @param kingPos Current position of the king
	 */
	public static final void getKingMoves(ArrayList<String> list, String kingPos){
		int x = Character.getNumericValue(kingPos.charAt(0)), y = Character.getNumericValue(kingPos.charAt(1));
		char kingColor = board[x][y].type.charAt(0);
		if((x+1 < 8 && y-1 >= 0) && (board[x+1][y-1] == null || board[x+1][y-1].type.charAt(0) != kingColor)) list.add((x+1)+""+(y-1));
		if(x+1 < 8 && (board[x+1][y] == null || board[x+1][y].type.charAt(0) != kingColor)) list.add((x+1)+""+y);
		if((x+1 < 8 && y+1 < 8) && (board[x+1][y+1] == null || board[x+1][y+1].type.charAt(0) != kingColor)) list.add((x+1)+""+(y+1));
		if(y-1 >= 0 && (board[x][y-1] == null || board[x][y-1].type.charAt(0) != kingColor)) list.add(x+""+(y-1));
		if(y+1 < 8 && (board[x][y+1] == null || board[x][y+1].type.charAt(0) != kingColor)) list.add(x+""+(y+1));
		if((x-1 >= 0 && y-1 >= 0) && (board[x-1][y-1] == null || board[x-1][y-1].type.charAt(0) != kingColor)) list.add((x-1)+""+(y-1));
		if(x-1 >= 0 && (board[x-1][y] == null || board[x-1][y].type.charAt(0) != kingColor)) list.add((x-1)+""+y);
		if((x-1 >= 0 && y+1 < 8) && (board[x-1][y+1] == null || board[x-1][y+1].type.charAt(0) != kingColor)) list.add((x-1)+""+(y+1));
	}
	
}
