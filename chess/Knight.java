package chess;

/**
 * Knight subclass
 * 
 * @author Jon Cobi Delfin and Luis Guzman
 */

public class Knight extends ChessPiece {

	/**
	 * Creates a knight chess piece
	 * @param type Type of the knight, can be 'w' or 'b'.
	 * @param location Current location of the knight. Coordinates are stored as rowcolumn instead of filerank
	 */
	public Knight(String type, String location) {
		// type = bKnight or wKnight
		super(type, location);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean movePiece(String oldLocation, String newLocation) {
		// Original Coordinates
		String s1 = convertInput(oldLocation);
		int x1 = Character.getNumericValue(s1.charAt(0)), y1 = Character.getNumericValue(s1.charAt(1));
		ChessPiece origin = ChessPiece.getPiece(s1);
		
		// New Coordinates
		String s2 = convertInput(newLocation);
		int x2 = Character.getNumericValue(s2.charAt(0)), y2 = Character.getNumericValue(s2.charAt(1));
		ChessPiece obj = ChessPiece.getPiece(s2);

		boolean isValid = false;
		
		// wKnight and bKnight both have the same functionality, the code below works for both types
		if((x1+2 == x2 || x1-2 == x2) && (y1+1 == y2 || y1-1 == y2)) {
			// 2 Up / down then 1 right / left
			isValid = true;
		}
		else if((x1+1 == x2 || x1-1 == x2) && (y1+2 == y2 || y1-2 == y2)) {
			// 2 right / left then 1 up / down
			isValid = true;
		}
		
		// Update board location if valid
		if(isValid) {
			// Check if newLocation contains opponents piece
			if(onSameTeam(origin, obj) == false) {
//				System.out.println("Valid Move");
				if(obj != null) {
//					System.out.println("Opponent Captured!");
				}
				ChessPiece.updateBoard(s1, s2);
				return true;
			}
			else {
//				System.out.println("Invalid Move, cannot capture your own team");
				return false;
			}
		}
		else {
//			System.out.println("Invalid Move. Within Knight Class...");
			return false;
		}
	}
}
