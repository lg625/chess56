package chess;

/**
 * Pawn subclass
 * 
 * @author Jon Cobi Delfin and Luis Guzman
 */

public class Pawn extends ChessPiece{
	/**
	 * Number of moves the pawn has moved. Used in determining first move distance and enpassant.
	 */
	public int moveCount;
	/**
	 * The turn that a pawn is currently on.
	 */
	public int turnMoved;
	/**
	 * Creates a pawn chess piece
	 * @param type Type of the pawn, can be 'w' or 'b'.
	 * @param location Current location of the pawn. Coordinates are stored as rowcolumn instead of filerank
	 */
	public Pawn(String type, String location) {
		// type = bp or wp
		super(type, location);
		this.moveCount = 0;
		this.turnMoved = ChessPiece.getTurn();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean movePiece(String oldLocation, String newLocation) {
		// Original Coordinates
		String s1 = convertInput(oldLocation);
		int x1 = Character.getNumericValue(s1.charAt(0)), y1 = Character.getNumericValue(s1.charAt(1));
		ChessPiece origin = ChessPiece.getPiece(s1);
		
		// New Coordinates
		String s2, promotionInput = null;
		if(newLocation.length() == 3) {
			s2 = convertInput(newLocation.substring(0, 2));
			promotionInput = newLocation.substring(2);
		}
		else
			s2 = convertInput(newLocation);
		int x2 = Character.getNumericValue(s2.charAt(0)), y2 = Character.getNumericValue(s2.charAt(1));
		ChessPiece obj = ChessPiece.getPiece(s2);
		
		//System.out.println("(x1,y1) = " + x1 + "," + y1 + "\t(x2,y2) = " + x2 + "," + y2);

		// Check if move is valid
		boolean isValid = false, isFirstMove = moveCount == 0 ? true : false;
		String enpassantOutput = isEnpassant(origin, newLocation);
		
		if(origin.type == "wp") {
			if((x1-1 == x2 && y1 == y2) || (x1-2 == x2 && y1 == y2 && isFirstMove)) {
				// 1 Forward Move or 2 Forward Moves
				// Cannot capture opponent directly in front, can only move forward if space is null
				if(obj == null)
					isValid = true;
			}
			else if(x1-1 == x2 && (y1-1 == y2 || y1+1 == y2)){
				// 1 Forward Diagonal Move -> only possible if opponent piece is there or enpassant cases are true
				if(obj != null || enpassantOutput != null)
					isValid = true;
			}
		}
		else if(origin.type == "bp") {
			if((x1+1 == x2 && y1 == y2) || (x1+2 == x2 && y1 == y2 && isFirstMove)) {
				// 1 Forward Move or 2 Forward Moves
				// Cannot capture opponent directly in front, can only move forward if space is null
				if(obj == null)
					isValid = true;
			}
			else if(x1+1 == x2 && (y1-1 == y2 || y1+1 == y2)){
				// 1 Forward Diagonal Move -> only possible if opponent piece is there or enpassant cases are true
				if(obj != null || enpassantOutput != null)
					isValid = true;
			}
		}
		
		// Update board location if valid
		if(isValid) {
			if(onSameTeam(origin, obj) == false) {
//				System.out.println("Valid Move.");
				
				if(obj != null) {
//					System.out.println("Opponent Captured!");
				}else if(enpassantOutput != null) {
//					System.out.println("Opponent Captured! \nEnpassant move detected!");
					ChessPiece.removePiece(enpassantOutput);
				}
				
				if((origin.type == "wP" && x2 == 0) || (origin.type == "bP" && x2 == 7)) {
					// Check if pawn reached end of the board
//					System.out.println("Pawn Promotion Detected!");
					board[x1][y1] = promotePawn((Pawn)origin, promotionInput);
				}
				else {
					// Update Pawn Chess Piece
					Pawn t = (Pawn)origin;
					t.moveCount++;
					t.turnMoved = ChessPiece.getTurn();
				}
							
				ChessPiece.updateBoard(s1, s2);
				return true;
			}
			else {
//				System.out.println("Invalid Move, cannot capture your own team");
				return false;
			}
		}
		else {
//			System.out.println("Invalid Move.");
			return false;
		}
	}

	/**
	 * Checks neighbor to see if they're opposing pawn pieces with moveCount = 1
	 * @param origin Current coordinates of the pawn.
	 * @param newLocation Location where the pawn is to be moved.
	 * @return String Coordinates of the opponent to capture
	 */
	private String isEnpassant(ChessPiece origin, String newLocation) {
		int x1 = Character.getNumericValue(origin.location.charAt(0)), y1 = Character.getNumericValue(origin.location.charAt(1));
		int y2 = Character.getNumericValue(newLocation.charAt(1));
		ChessPiece neighbor;
		String output;
		
		if(y1-1 == y2) {
			// Right Neighbor
			neighbor = ChessPiece.getPiece(x1 + "" + (y1-1));
			output = x1 + "" + (y1-1);
		}
		else if (y1+1 == y2) {
			// Left Neighbor
			neighbor = ChessPiece.getPiece(x1 + "" + (y1+1));
			output = x1 + "" + (y1+1);
		}
		else
			return null;
		
		// Check if neighbor is a pawn from opposing team
		if(onSameTeam(origin, neighbor) == false && neighbor instanceof Pawn) {
			Pawn p = (Pawn)neighbor;
			
			// diagonal move only valid after 1st move, such that global turn - local turn = 1
			if(p.moveCount == 1 && (ChessPiece.getTurn()-p.turnMoved == 1))
				return output;
//			else
//				System.out.println("Invalid enpassant move attempted");
		}
		
		return null;
	}
	
	/**
	 * 
	 * @param Current coordinates of the pawn
	 * @param newType Chess piece that the pawn will become
	 * @return Chesspiece piece that the pawn will become
	 */
	private ChessPiece promotePawn(Pawn origin, String newType) {
		if(newType == null) {
			// Default; Promote to Queen Piece
			return new Queen((origin.type.charAt(0) + "Q"), origin.location);
		}
		
		if(newType.equals("R")) {
			// Promote to Rook Piece
			return new Rook((origin.type.charAt(0) + "R"), origin.location);
		}
		else if(newType.equals("N")) {
			// Promote to Knight Piece
			return new Knight((origin.type.charAt(0) + "N"), origin.location);
		} 
		else if(newType.equals("B")) {
			// Promote to Bishop Piece
			return new Bishop((origin.type.charAt(0) + "B"), origin.location);
		} 
		else if(newType.equals("Q")) {
			// Promote to Queen Piece
			return new Queen((origin.type.charAt(0) + "Q"), origin.location);
		} 
		else {
			// Invalid Chess Piece Type, return origin
			return origin;
		}
	}
}
